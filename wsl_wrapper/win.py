'''The code that runs on Windows'''

import os
import sys
import shutil

from subprocess import Popen, PIPE
from os.path import join, exists


class WinProc:
    def __init__(self, path, vm_name='Ubuntu'):
        self.path = path
        self.wsl_root = '\\\\wsl$'
        self.vm_name = vm_name
        self.get_wsl_path()

        self.tmpdir = join(self.wsl_root, self.vm_name, 'tmp\\wsl_wrapper')
        if not exists(self.tmpdir):
            os.mkdir(self.tmpdir)
        else:
            for f in os.listdir(self.tmpdir):
                os.remove(join(self.tmpdir, f))

        self.cp_files_local()
        self.launch_wsl()
        self.cp_files_remote()


    def get_wsl_path(self):
        cmd = Popen(['cmd.exe', '/c', 'where ubuntu.exe'], 
                    stdout=PIPE, stderr=PIPE)
        out, err = cmd.communicate()
        if err:
            print(str(err, 'utf-8'), file=sys.stderr)
            self.wsl_path = 'ubuntu.exe'
        if out:
            out = str(out, 'utf-8').strip()
            self.wsl_path = out

    def cp_files_local(self):
        self.new_files = os.listdir(self.path)
        for f in self.new_files:
            if not f.endswith('.pdf'):
                self.new_files.remove(f)
                continue
            else:
                try:
                    pf = join(self.path, f)
                    pt = join(self.tmpdir, f)
                    shutil.copy(pf, pt)
                except:
                    print('error on file %s' % f)

    def cp_files_remote(self):
        for f in self.new_files:
            if not f.endswith('.txt'):
                continue
            else:
                try:
                    pf = join(self.tmpdir, f)
                    pt = join(self.path, f)
                    shutil.copy(pf, pt)
                except:
                    print('error on file %s' % f)

    def launch_wsl(self):
        with Popen([self.wsl_path, 'run', 'python3', 
                        '~/.local/bin/wsl_wrapper_process'],
                    stdout=PIPE, stderr=PIPE
                ) as proc:
            out, err = proc.communicate()
            if err:
                print(str(err, 'utf-8'), file=sys.stderr)
            if out:
                print(str(out, 'utf-8'))
