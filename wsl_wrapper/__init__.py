import sys


from .win import *

if not 'win' in sys.platform:
    from .wsl import *
