'''the code that runs on WSL through Popen from Windows'''


import os
import pdftotext as p2t

from os.path import join, exists


class WSLProc:
    def __init__(self):
        self.tmpdir = '/tmp/wsl_wrapper'
        if not exists(self.tmpdir):
            os.mkdir(self.tmpdir)

        self.convert()


    def convert(self):
        for f in os.listdir(self.tmpdir):
            if not f.endswith('.pdf'):
                continue
            else:
                fp = join(self.tmpdir, f)
                o = join(self.tmpdir, os.path.splitext(f)[0] + '.txt')
                with open(fp, 'rb') as fh, open(o, 'w') as fh_o:
                    try:
                        pdf = p2t.PDF(fh)
                        text = "".join( page for page in pdf )
                        fh_o.write(text)
                        print('[+] successfully processed %s' % f)
                    except:
                        print('[-] failed to process %s' % f)
                        pass
