# WSL Wrapper for pdftotext

This module is for those who want to convert pdfs to non-horrible looking txt files in Windows.  In Linux, this is simple with `pdftotext`, but installing this program on Windows is not easy and results are poor even if you manage to install it.  WSL is a partial solution, but it's difficult to map Sharepoint drives in WSL.  There are also many people who are not comfortable using a Linux terminal.

This module solves these problems by automating the process of copying remote files to the C drive, and then launching WSL to process them with `pdftotext`, before copying the resulting txt files back to the remote drive.

The user must have WSL installed, and must ensure that this module is installed on both Windows and WSL.  This module should install `pdftotext` for you on Linux.

## Installation

Firstly, you need to install WSL (Ubuntu) and create a user with sudo privileges ([this should happen by default](https://docs.microsoft.com/en-us/windows/wsl/setup/environment#set-up-your-linux-username-and-password)).  When you've done that, you need to install this package on both WSL and on Windows.  This package assumes that you are using the `Ubuntu` version of WSL.

Python and git will not be installed by default, to install them:

        sudo apt update && sudo apt upgrade
        sudo apt install git python3 python3-pip

> You will be prompted for your password, it is normal for nothing to show up on the screen while you are entering it, finish typing it and press enter.

You will need to follow the proceeding instructions on WSL and then Windows:

### Instructions

#### On WSL

First, clone this repo:

        git clone https://gitlab.com/sean-c/wsl-wrapper
        cd wsl-wrapper

Execute the following command (as your sudo user, not root):

        python3 setup.py install --user

This will install `wsl-wrapper` in WSL, and will also prompt you for your password in order to install poppler (this is why sudo privileges are important).

Finally, run:

        powershell.exe .

To get a Windows shell in the same directory.

#### On Windows

Now simply install the package in Windows and you're done:

        C:\\path\to\your\python\install\python.exe setup.py install --user

## Usage

### From Python

Executing the following code from windows will convert all pdf files in the current directory to txt:

        from wsl_wrapper import WinProc

        WinProc('.')

You can replace `'.'` with any Windows path.

