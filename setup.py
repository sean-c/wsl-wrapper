import sys
import os

from setuptools import setup
from setuptools.command.install import install


class CustomInstall(install):
    def run(self):
        install.run(self)

        if 'win' in sys.platform:
            return None

        i = input('''
                Would you like to set up pdftotext now? (Y/n)

                This is recomended if you are installing this
                in a fresh WSL install, or if you don't have
                pdftotext installed.

                ''')
        if i not in ['n', 'N']:
            print('\nRunning WSL setup script requires your password:')
            os.system('sudo ./setup_wsl.sh')


def req():
    if sys.platform == 'linux':
        return ['pdftotext']
    elif sys.platform.startswith('win'):
        return []
    else:
        return ['pdftotext']


setup(
        name='wsl_wrapper',
        version='1.0',
        description='launch WSL from Windows to convert PDFs',
        packages=['wsl_wrapper'],
        scripts=['bin/wsl_wrapper_process'],
        install_requires=req(),
        cmdclass={'install': CustomInstall}
)
