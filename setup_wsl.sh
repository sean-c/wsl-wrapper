#!/usr/bin/env bash

[[ $EUID -ne 0 ]] && echo "This script must be run as root. Try sudo wsl_setup.sh" && exit 1

if [[ ! -f $HOME/.local/src ]]
then
        mkdir $HOME/.local/src
fi
cd $HOME/.local/src

sudo apt install g++ autoconf libfontconfig1-dev pkg-config libjpeg-dev gnome-common libglib2.0-dev gtk-doc-tools libyelp-dev yelp-tools gobject-introspection libsecret-1-dev libnautilus-extension-dev

wget https://poppler.freedesktop.org/poppler-data-0.4.7.tar.gz
tar -xf poppler-data-0.4.7.tar.gz
cd poppler-data-0.4.7
sudo make install
cd ..

wget https://poppler.freedesktop.org/poppler-0.48.0.tar.xz
tar -xf poppler-0.48.0.tar.xz
cd poppler-0.48.0
./configure
make
sudo make install

sudo ln -s /usr/local/bin/pdftotext /usr/bin/pdftotext
sudo ln -s /usr/local/bin/pdftoppm /usr/bin/pdftoppm
sudo ln -s /usr/local/lib/libpoppler.so.64 /usr/lib/libpoppler.so.64
